variable "namespace" {
  type        = "string"
  default     = "sk"
  description = "Organization namespace"
}

variable "stage" {
  type        = "string"
  description = "Stage (short name), e.g. 'prd', 'stg', 'tst', 'dev'"
}

variable "environment" {
  description = "Stage (long name), e.g. 'production', 'staging', 'testing', 'development "
}

variable "name" {
  type        = "string"
  default     = "webapp"
  description = "Solution name"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "tags" {
  type = "map"

  default = {
    "Owner"  = "SkaleSys"
    "Office" = "Perth"
  }

  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "region" {
  type        = "string"
  default     = "ap-southeast-2"
  description = "AWS Region"
}

variable "acm_certificate_arn" {
  description = "Existing ACM Certificate ARN"
  default     = "arn:aws:acm:us-east-1:096373988534:certificate/ade5f55d-286d-45b3-9f38-e57c8b7625ca"
}

variable "main_domain" {
  description = "Domain name"
  default     = "skalesys.com"
}
