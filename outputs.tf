output "cdn_arn" {
  description = "ARN of AWS CloudFront distribution"
  value       = "${module.cdn.cf_arn}"
}

output "cdn_domain_name" {
  description = "Domain name corresponding to the distribution"
  value       = "${module.cdn.cf_domain_name}"
}

output "cdn_etag" {
  description = "Current version of the distribution's information"

  value = "${module.cdn.cf_etag}"
}

output "cdn_hosted_zone_id" {
  description = "CloudFront Route 53 zone ID"

  value = "${module.cdn.cf_hosted_zone_id}"
}

output "cdn_id" {
  description = "ID of AWS CloudFront distribution"

  value = "${module.cdn.cf_id}"
}

output "cdn_status" {
  description = "Current status of the distribution"

  value = "${module.cdn.cf_status}"
}

output "cdn_bucket" {
  description = "Name of S3 bucket"

  value = "${module.cdn.s3_bucket}"
}

output "cdn_bucket_domain_name" {
  description = "Domain of S3 bucket"

  value = "${module.cdn.s3_bucket_domain_name}"
}
